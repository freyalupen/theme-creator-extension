from .theme_creator import Theme_creator

# And add the extension to Krita's list of extensions:
app = Krita.instance()
# Instantiate your class:
extension = Theme_creator(parent = app)
app.addExtension(extension)
