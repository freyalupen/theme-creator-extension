# Theme Creator
# Copyright (C) 2023 Freya Lupen <penguinflyer2222@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from krita import Extension
try:
    from PyQt6.QtGui import QPalette, QColor
    from PyQt6.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, \
                                QVBoxLayout, QHBoxLayout, QGridLayout, \
                                QDialog, QFileDialog, QMessageBox, QColorDialog
    from PyQt6.QtCore import QFileInfo, pyqtSignal, Qt
except:
    from PyQt5.QtGui import QPalette, QColor
    from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, \
                                QVBoxLayout, QHBoxLayout, QGridLayout, \
                                QDialog, QFileDialog, QMessageBox, QColorDialog
    from PyQt5.QtCore import QFileInfo, pyqtSignal, Qt

from .kconfig_parser import KConfigHandler

import re

EXTENSION_ID = 'pykrita_theme_creator'
MENU_ENTRY = 'Theme Creator'

PALETTE_VALUES = [
    QPalette.ColorRole.Window,
    QPalette.ColorRole.WindowText,
    QPalette.ColorRole.Base,
    QPalette.ColorRole.Text,
    QPalette.ColorRole.Button,
    QPalette.ColorRole.ButtonText,
    QPalette.ColorRole.Highlight,
    QPalette.ColorRole.HighlightedText,
    QPalette.ColorRole.ToolTipBase,
    QPalette.ColorRole.ToolTipText,
    QPalette.ColorRole.AlternateBase,
    QPalette.ColorRole.Link,
    QPalette.ColorRole.LinkVisited
]
SCHEME_VALUES = [
    "Window: Background", # 0
    "Window: Foreground",
    "View: Background",
    "View: Foreground",
    "Button: Background",
    "Button: Foreground", # 5
    "Selection: Background",
    "Selection: Foreground",
    "Tooltip: Background *",
    "Tooltip: Foreground *",
    "View: BackgroundAlternate *", # 10
    "View: ForegroundLink *",
    "View: ForegroundVisited *"
]

TOOLTIP_TEXT = [
    "Background for the main, nonselectable window elements.",
    "Text for the main, nonselectable window elements.",
    "Background for containers of selectable elements, like menus, " + \
        "or unselected elements, like the background of empty sliders.",
    "Text for containers of selectable elements, like menus, " + \
        "or unselected elements, like the background of empty sliders.",
    "Background for buttons, dropdowns, and scrollbars.",
    "Text for buttons, dropdowns, and scrollbars.",
    "Background for selectable elements, like the filled part of sliders, " + \
        "or selected elements like menu options.",
    "Text for selectable elements, like the filled part of sliders, " + \
        "or selected elements like menu options.",
    "Background for tooltips. *Not previewable. May not get used.",
    "Text for tooltips. *Not previewable. May not get used.",
    "Second background for lists with alternating row colors, like " + \
        "Preferences->Keyboard Shortcuts. *Not previewable.",
    "URL link text, used in Welcome Page and About pages. *Not previewable.",
    "Visited link text. *Not previewable. May not be used?"
]

SCHEME_COLORS = [
        ["Colors:Window", "BackgroundNormal", "214,210,208"],
        ["Colors:Window", "ForegroundNormal", "34,31,30"],
        ["Colors:View", "BackgroundNormal", "255,255,255"],
        ["Colors:View", "ForegroundNormal", "31,28,27"],
        ["Colors:Button", "BackgroundNormal", "223,220,217"],
        ["Colors:Button", "ForegroundNormal", "34,31,30"],
        ["Colors:Selection", "BackgroundNormal", "67,172,232"],
        ["Colors:Selection", "ForegroundNormal", "255,255,255"],
        ["Colors:Tooltip", "BackgroundNormal", "24,21,19"],
        ["Colors:Tooltip", "ForegroundNormal", "231,253,255"],
        ["Colors:View", "BackgroundAlternate", "248,247,246"],
        ["Colors:View", "ForegroundLink", "0,87,174"],
        ["Colors:View", "ForegroundVisited", "100,74,155"]
]

def qcolorToRGB(color):
    return '%d,%d,%d' % (color.red(), color.green(), color.blue())
def RGBtoColorName(string):
    rgb = string.split(',')
    return QColor(int(rgb[0]), int(rgb[1]),int(rgb[2])).name()

class ColorLineEdit(QLineEdit):
    colorUpdated = pyqtSignal()

    def __init__(self, parent):
        super().__init__(parent)

    def setColor(self, color):
        self.setText(color.name())
        self.colorUpdated.emit()

    def color(self):
        text = self.text()
        if QColor.isValidColor(text):
            return QColor(text)
        else:
            # Support comma-separated RGB format:
            rgb = re.fullmatch("(\d{1,3}),(\d{1,3}),(\d{1,3})", text)
            if rgb:
                return QColor(int(rgb[1]), int(rgb[2]), int(rgb[3]))
            else:
                return QColor()

    def showColorPicker(self):
        oldColor = self.color()
        colorPicker = QColorDialog(oldColor)
        colorPicker.currentColorChanged.connect(self.setColor)
        result = colorPicker.exec()
        if result == QDialog.DialogCode.Rejected:
            self.setColor(oldColor)

class Theme_creator(Extension):

    def __init__(self, parent):
        super().__init__(parent)

        self.firstRun = True

    def setup(self):
        pass

    def createActions(self, window):
        themeAction = window.createAction(EXTENSION_ID, MENU_ENTRY, "tools/scripts")
        themeAction.triggered.connect(self.openDialog)

    def openDialog(self):
        if self.firstRun:
            self.initialize()
        self.showDialog()

    # Set up things only when the dialog is opened for the first time.
    def initialize(self):
        self.firstRun = False

        (majorVer, minorVer, tmp) = Application.version().split(".")
        # Check whether this function is available, it was added in 5.1.0.
        if int(majorVer) >= 5 and int(minorVer) >= 1:
            self.currentFolder = Application.getAppDataLocation() + "/color-schemes"
        else:
            # The same thing but less convenient.
            self.currentFolder = Application.readSetting("", "resourceDirectory", \
                QStandardPaths.writableLocation(QStandardPaths.AppDataLocation)) + "/color-schemes"

        self.newColors = [0 for i in range(len(PALETTE_VALUES))]
        self.schemeName = "Current Theme"
        self.source = "current palette + default theme"

        self.themeConfig = KConfigHandler()
        # Find the default config that should be in the same folder as this script.
        defaultThemePath = QFileInfo(__file__).path()+"/DefaultTheme.colors"
        success = self.themeConfig.parseConfig(defaultThemePath)
        if not success:
            print("Theme Creator Extension: Error: Could not open default theme!")
            self.source = "current palette"

        self.createDialog()

    def getCurrentPalette(self):
        palette = Application.activeWindow().qwindow().palette()

        for i, role in zip(range(len(PALETTE_VALUES)), PALETTE_VALUES):
            self.newColors[i] = palette.color(role)

    def setName(self):
        self.schemeName = self.schemeNameEdit.text()

    # Create the dialog --
    def createDialog(self):
        self.getCurrentPalette()

        layout = QVBoxLayout()

        nameLayout = QHBoxLayout()
        self.schemeNameEdit = QLineEdit(self.schemeName)
        nameLabel = QLabel("Name:")
        nameLabel.setToolTip("The name that will be shown in the Themes menu.")
        nameLayout.addWidget(nameLabel)
        nameLayout.addWidget(self.schemeNameEdit)
        self.schemeNameEdit.editingFinished.connect(self.setName)
        layout.addLayout(nameLayout)

        gridLayout = QGridLayout()

        row = 0
        for color, colorName, tooltip in zip(self.newColors, SCHEME_VALUES, TOOLTIP_TEXT):
            lineEdit = ColorLineEdit(color.name())
            lineEdit.setObjectName(colorName)
            lineEdit.editingFinished.connect(self.updateColors)
            lineEdit.colorUpdated.connect(self.updateColors)
            label = QLabel(colorName)
            label.setToolTip(tooltip)
            gridLayout.addWidget(label, row,0)
            gridLayout.addWidget(lineEdit, row,1)

            colorButton = QPushButton()
            colorButton.setObjectName(colorName+" Button")
            colorButton.clicked.connect(lineEdit.showColorPicker)
            gridLayout.addWidget(colorButton, row,2)

            row += 1

        layout.addLayout(gridLayout)

        # Lazy way to add separation.
        layout.addWidget(QLabel())

        sourceLayout = QHBoxLayout()
        sourceLabel = QLabel("Loaded from:")
        sourceLabel.setToolTip("Where the theme was loaded from. " + \
                                "This will be used as the source of "+ \
                                "all other configuration properties when saving.")
        sourceLayout.addWidget(sourceLabel)
        self.sourceWidget = QLineEdit(self.source)
        self.sourceWidget.setReadOnly(True)
        sourceLayout.addWidget(self.sourceWidget)

        importButton = QPushButton()
        importButton.setIcon(Application.icon("document-open"))
        importButton.setToolTip("Choose a file")
        importButton.pressed.connect(self.openThemeFile)
        sourceLayout.addWidget(importButton)
        layout.addLayout(sourceLayout)

        saveButton = QPushButton("Save theme...")
        saveButton.setToolTip("Save these values to a theme file.")
        saveButton.pressed.connect(self.saveThemeFile)
        layout.addWidget(saveButton)

        self.dialog = QDialog(Application.activeWindow().qwindow())

        # Don't keep Krita from closing
        self.dialog.setAttribute(Qt.WidgetAttribute.WA_QuitOnClose, False)

        closeButton = QPushButton("Close")
        closeButton.setDefault(True)
        closeButton.clicked.connect(self.dialog.accept)
        layout.addWidget(closeButton)

        self.dialog.setLayout(layout)

        self.updateColors()

        self.dialog.setWindowTitle("Theme Creator") 

    def showDialog(self):
        self.dialog.show()

    # -- Painting --
    def paintUI(self):
        window = Application.activeWindow().qwindow()
        newPalette = window.palette()

        for color, colorRole in zip(self.newColors, PALETTE_VALUES):
            if color.isValid():
                newPalette.setColor(QPalette.ColorGroup.Active, colorRole, color)
                # Inactive and Disabled use special effects,
                # but those are hard to replicate, so use the Active color.
                newPalette.setColor(QPalette.ColorGroup.Inactive, colorRole, color)
                newPalette.setColor(QPalette.ColorGroup.Disabled, colorRole, color)

        window.setPalette(newPalette)

        # Setting only the window palette has limitations,
        # so try and set some widgets too.
        # For some reason, this findChildren call prints out two warnings:
        # "QObject::connect: No such signal QQmlSizeValueType::destroyed(QObject *)
        #  QObject::connect: No such signal QQuickFontValueType::destroyed(QObject *)"
        print("Theme Creator Extension: Finding widgets. " + \
              "Expect some missing signals:")
        childWidgets = window.findChildren(QWidget)
        for child in childWidgets:
            child.setPalette(newPalette)

    def updateColors(self):
        for i, colorName in zip(range(len(self.newColors)), SCHEME_VALUES):
            lineEdit = self.dialog.findChild(ColorLineEdit, colorName)
            self.newColors[i] = lineEdit.color()
        self.paintUI()
        self.paintColorButtons()

    def paintColorButtons(self):
        for color, colorName in zip(self.newColors, SCHEME_VALUES):
            colorButton = self.dialog.findChild(QPushButton, colorName+" Button")
            buttonPalette = QPalette()
            buttonPalette.setColor(QPalette.ColorRole.Button, color)
            colorButton.setPalette(buttonPalette)

    # Theme file handling--
    def openThemeFile(self):
        path, _filter = QFileDialog.getOpenFileName(None, "Open a Theme file", \
                                    filter="Color theme (*.colors)", directory=self.currentFolder)
        if not path:
            return
        self.currentFolder = QFileDialog().directoryUrl().toLocalFile()

        self.themeConfig = KConfigHandler()
        success = self.themeConfig.parseConfig(path)
        if not success:
            resultBox = QMessageBox(Application.activeWindow().qwindow())
            resultBox.setText("Failed to load %s." % path)
            resultBox.setIcon(QMessageBox.Icon.Warning)
            resultBox.show()
            self.source = "none"
            return
        self.loadTheme()
        self.source = path
        self.sourceWidget.setText(self.source)

    def loadTheme(self):
        for i, colorName, config in zip(range(len(self.newColors)), SCHEME_VALUES, SCHEME_COLORS):
            lineEdit = self.dialog.findChild(ColorLineEdit, colorName)
            lineEdit.setText(RGBtoColorName(self.themeConfig.getValue(config[0],config[1],config[2])))
        self.updateColors()

        self.schemeName = self.themeConfig.getValue("General", "Name", "")
        self.schemeNameEdit.setText(self.schemeName)

    def saveThemeFile(self):
        fileDialog = QFileDialog(filter="Color theme (*.colors)", directory=self.currentFolder)
        fileDialog.setAcceptMode(QFileDialog.AcceptMode.AcceptSave)
        # Automatically add the required extension if it wasn't specified by the user
        fileDialog.setDefaultSuffix("colors")
        if not fileDialog.exec():
            return
        path = fileDialog.selectedFiles()[0]
        if not path:
            return
        self.currentFolder = QFileDialog().directoryUrl().toLocalFile()

        for config, color in zip(SCHEME_COLORS, self.newColors):
            self.themeConfig.setValue(config[0],config[1], qcolorToRGB(color))
        self.themeConfig.setValue("General", "ColorScheme", self.schemeName)
        self.themeConfig.setValue("General", "Name", self.schemeName)
        success = self.themeConfig.writeConfig(path)

        resultBox = QMessageBox(self.dialog)
        resultText = "Successfully saved theme '%s' to \"%s\"" % (self.schemeName, path) \
                     if success else "Failed to save theme '%s' to \"%s\"." % (self.schemeName, path)
        resultIcon = QMessageBox.Icon.NoIcon if success else QMessageBox.Icon.Warning
        resultBox.setText(resultText)
        resultBox.setIcon(resultIcon)
        resultBox.show()

        if success:
            self.source = path
            self.sourceWidget.setText(self.source)
