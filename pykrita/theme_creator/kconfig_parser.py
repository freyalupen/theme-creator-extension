# KConfig Parser
# Copyright (C) 2023 Freya Lupen <penguinflyer2222@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

try:
    from PyQt6.QtCore import QFile, QIODevice, QFileInfo, QMimeDatabase
except:
    from PyQt5.QtCore import QFile, QIODevice, QFileInfo, QMimeDatabase


def trim(line):
    # Remove whitespace, carriage return, tab,
    # as per KConfig's bufferfragment_p.h.
    return line.rstrip(" \r\t")

# A basic parser for reading/writing KConfig files.
# It doesn't support options like [$i], [$e], [$d].
class KConfigHandler:

    def __init__(self):
        self.configGroups = {"<default>":0}
        self.configValues = [{}]
        self.loggingEnabled = True
        self.failOnParseIssue = True

    def log(self, message):
        if self.loggingEnabled:
            print("kconfig_parser.py: " + message)

    def parseConfig(self, path):
        # Check if the file is readable, read the file --
        if not path:
            return False

        if not QFileInfo(path).exists():
            return False

        mimeType = QMimeDatabase().mimeTypeForFile(path)
        if not mimeType.inherits("text/plain"):
            return False

        file = QFile(path)
        if not file.open(QIODevice.OpenModeFlag.ReadOnly):
            return False
        
        data = file.readAll()
        file.close()
        data = f"{str(data, 'utf-8')}"

        # Now parse it --
        # - Adapted from KConfig's KConfigIniBackend::parseConfig
        # - (https://invent.kde.org/frameworks/kconfig/-/blob/master/src/core/kconfigini.cpp :
        # -  Copyright Matthias Kalle Dalheimer, Preston Brown, Thomas Braxton
        # -  Licensed under LGPL-2.0-or-later)
        invalidErrors = 0
        unsupportedOptions = 0
        lineNum = 0
        currentGroup = "<default>"
        lines = data.split("\n")
        for line in lines: # ReadLines
            lineNum += 1
            gotoNextLine = False
            line = trim(line)

            # Skip empty lines and comments.
            if line == "" or line[0] == "#":
                continue

            if line[0] == '[': # ReadGroups
                newGroup = ""
                start = 1
                end = 0
                while 1: # DoWhile
                    end = start
                    while 1:
                        if end == len(line):
                            self.log("Invalid group header @ line %d." % lineNum)
                            invalidErrors += 1
                            gotoNextLine = True
                            break
                        if line[end] == ']':
                            break
                        end += 1
                    if gotoNextLine:
                        break

                    if end + 1 == len(line) and start + 2 == end \
                       and line[start:start+1] == '$i':
                        self.log("Unsupported $i option @ line %d." % lineNum)
                        unsupportedOptions += 1
                        pass
                    else:
                        # Add the group name.
                        if not newGroup == "":
                            newGroup += "\x1d" # "Group Separator"
                        newGroup += line[start:end]
                    
                    start = end + 2
                    if not (start < len(line) and line[end+1] == '['):
                        break
                # End DoWhile
                if gotoNextLine:
                    break
                
                currentGroup = newGroup
                self.addGroup(currentGroup)
            # End ReadGroups
            else: # ReadKeyValue
                eqpos = line.find("=")
                temp = line.split("=", maxsplit = 1)
                key = trim(temp[0])
                value = trim(temp[1]) if len(temp) == 2 else ""
                if not key:
                    self.log("Invalid entry (empty key) @ line %d." % lineNum)
                    invalidErrors += 1
                    continue
    
                # Search for keys with options like key[$e] (not supported)
                start = key.rfind('[')
                while start >= 0: # KeyWhile
                    end = key.find(']', start)
                    if end < 0:
                        self.log("Invalid entry (missing ']') @ line %d." % lineNum)
                        invalidErrors += 1
                        gotoNextLine = True
                        break
                    elif (end > (start + 1)) and key[start+1] == '$':
                        self.log("Unsupported %s option @ line %d." % (key,lineNum))
                        unsupportedOptions += 1
                        pass
                    else:
                        self.log("Unsupported locale option @ line %d." % lineNum)
                        unsupportedOptions += 1
                        pass
                    key = key[:start]
                    
                    start = key.rfind('[')
                # End KeyWhile

                if gotoNextLine:
                    break

                if eqpos < 0:
                    self.log("Invalid entry (missing '=') @ line %d." % lineNum)
                    invalidErrors += 1
                    continue

                self.configValues[-1][key] = value
                # EndReadKeyValue
             
            if gotoNextLine:
                continue
        #End ReadLines
        # - (End parseConfig snippet)

        if invalidErrors:
            self.log("Had %d errors parsing %s." % (invalidErrors, path))
        if unsupportedOptions:
            self.log("Encountered %d unsupported options parsing %s." % (unsupportedOptions, path))
        
        # Todo: Return useful errors instead of just False
        if self.failOnParseIssue:
            if invalidErrors or unsupportedOptions:
                return False
        return True

    def getValue(self, group, key, default):
        if group in self.configGroups:
            idx = self.configGroups[group]
            if key in self.configValues[idx]:
                return self.configValues[idx][key]
        return default

    def setValue(self, group, key, value):
        if not group in self.configGroups:
            self.addGroup(group)
        idx = self.configGroups[group]
        self.configValues[idx][key] = value

    def addGroup(self, group):
        self.configGroups[group] = len(self.configValues)
        self.configValues.append({})

    # Write the file, doesn't support options.
    # Todo: The order of the groups and the placement of newlines isn't "correct".
    def writeConfig(self, path):
        if not path:
            return False
        file = QFile(path)
        if not file.open(QIODevice.WriteOnly):
            return False
        
        output = ""
        for group in self.configGroups:
            idx = self.configGroups[group]
            if idx != 0: # 0 is <default>
                subgroups = group.split("\x1d")
                for sub in subgroups:
                    output += "[%s]" % sub
                output += "\n"
            for key in self.configValues[idx]:
                output += "%s=%s\n" % (key, self.configValues[idx][key])
            if idx != 0 and idx != len(self.configValues)-1:
                output += "\n"

        success = file.write(output.encode('utf-8'))
        file.close()

        return success